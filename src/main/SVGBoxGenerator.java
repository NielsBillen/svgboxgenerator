package main;

import gui.SizeFrame;

import java.util.List;

import svg.SVG;
import svg.SVGUnit;
import box.BoxGenerator;
import core.Box;
import core.Polygon;

/**
 * Entry point for the box generation class.
 * 
 * @author Niels Billen
 * @version 1.0
 */
public class SVGBoxGenerator {
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] arguments) {
		double width = Double.NaN;
		double height = Double.NaN;
		double depth = Double.NaN;
		double thickness = Double.NaN;

		if (arguments.length == 0) {
			new SizeFrame();
			return;
		} else {
			for (int i = 0; i < arguments.length; ++i) {
				if (arguments[i].equals("-width"))
					width = Double.parseDouble(arguments[++i]);
				else if (arguments[i].equals("-height"))
					height = Double.parseDouble(arguments[++i]);
				else if (arguments[i].equals("-depth"))
					depth = Double.parseDouble(arguments[++i]);
				else if (arguments[i].equals("-thickness"))
					thickness = Double.parseDouble(arguments[++i]);
				else if (arguments[i].equals("-help")) {
					System.out
							.println("usage:\n\t"
									+ "-width: specify the width of the box (in mm)\n\t"
									+ "-height: specify the height of the box (in mm)\n\t"
									+ "-depth: specify the depth of the box (in mm)\n\t"
									+ "-thickness: specify the thickness of the boxy (in mm)\n\t"
									+ "-help: print this help message.\n\n"
									+ "example usage:\n\t"
									+ "java -jar SVGBoxGenerator.jar "
									+ "-width 100 "
									+ "-height 50 "
									+ "-depth 200 "
									+ "-thickness 4\n\n"
									+ "note: all of the sizes have to be specified in order for the program to work");
					System.out.flush();
				}
			}
			if (Double.isNaN(width)) {
				System.err.println("error: the width has not been set!");
				return;
			}
			if (Double.isNaN(height)) {
				System.err.println("error: the height has not been set!");
				return;
			}
			if (Double.isNaN(depth)) {
				System.err.println("error: the depth has not been set!");
				return;
			}
			if (Double.isNaN(thickness)) {
				System.err.println("error: the thickness has not been set!");
				return;
			}
		}

		List<Polygon> polygons = BoxGenerator.createBox(width, height, depth,
				thickness);
		int index = 0;
		for (Polygon polygon : polygons) {
			Box box = polygon.getSize();
			double ww = box.width() + 2 * thickness;
			double hh = box.height() + 2 * thickness;

			SVG svg = new SVG(ww, hh, SVGUnit.MM);
			svg.add(polygon.translate(thickness, thickness));
			svg.save(String.format("svg%d.svg", index));
			++index;
		}
	}
}
