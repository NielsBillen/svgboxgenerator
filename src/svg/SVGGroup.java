package svg;

import util.Indentation;
import core.Box;

/**
 * 
 * @author Niels Billen
 * @version 1.0
 */
public class SVGGroup extends SVGContainer {
	/**
	 * 
	 */
	public SVGGroup() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#toSVG(int)
	 */
	@Override
	public String toSVG(int indent) {
		String tabs = Indentation.getTabs(indent);
		StringBuilder builder = new StringBuilder(tabs);
		builder.append("<g>\n");
		builder.append(super.toSVG(indent + 1));
		builder.append("\n");
		builder.append(tabs);
		builder.append("</g>");
		return builder.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#translate(double, double)
	 */
	@Override
	public ISVGElement translate(double x, double y) {
		SVGGroup result = new SVGGroup();
		for (ISVGElement e : this)
			result.add(e.translate(x, y));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#rotate(double, double, double)
	 */
	@Override
	public ISVGElement rotate(double x, double y, double angle) {
		SVGGroup result = new SVGGroup();
		for (ISVGElement e : this)
			result.add(e.rotate(x, y, angle));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#getSize()
	 */
	@Override
	public Box getSize() {
		Box box = new Box();
		for (ISVGElement e : this)
			box = box.union(e.getSize());
		return box;
	}
}
