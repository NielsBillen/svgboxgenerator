package svg;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author Niels Billen
 * @version 1.0
 */
public abstract class SVGContainer implements ISVGElement {
	private List<ISVGElement> list = new ArrayList<ISVGElement>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#add(svg.ISVGElement)
	 */
	@Override
	public void add(ISVGElement element) throws UnsupportedOperationException {
		if (element == null)
			throw new NullPointerException("the given element is null!");
		list.add(element);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#remove(svg.ISVGElement)
	 */
	@Override
	public void remove(ISVGElement element)
			throws UnsupportedOperationException {
		list.remove(element);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#setStyle(svg.SVGStyle)
	 */
	@Override
	public void setStyle(SVGStyle style) {
		throw new UnsupportedOperationException("SVGElement of class \""
				+ getClass() + "\" does not support styling!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#getStyle()
	 */
	@Override
	public SVGStyle getStyle() {
		throw new UnsupportedOperationException("SVGElement of class \""
				+ getClass() + "\" does not support styling!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#toSVG(int)
	 */
	@Override
	public String toSVG(int indent) {
		StringBuilder builder = new StringBuilder();
		for (ISVGElement e : list) {
			builder.append(e.toSVG(indent));
			builder.append("\n");
		}
		return builder.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<ISVGElement> iterator() {
		return list.iterator();
	}
}
