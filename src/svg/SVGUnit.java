package svg;

/**
 * 
 * @author Niels Billen
 * @version 1.0
 */
public enum SVGUnit {
	MM, PX;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return name().toLowerCase();
	};
}
