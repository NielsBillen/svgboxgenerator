package svg;

import java.util.Locale;

/**
 * 
 * @author Niels Billen
 * @version 1.0
 */
public class SVGStyle {
	private String fill = "none";
	private String stroke = "red";
	private double strokeWidth = 0.05;
	private SVGUnit strokeWidthUnit = SVGUnit.MM;

	public final static SVGStyle DEFAULT = new SVGStyle();

	/**
	 * 
	 */
	public SVGStyle() {
	}

	/**
	 * 
	 * @param fill
	 * @param stroke
	 * @param strokeWidth
	 * @param strokeUnit
	 */
	public SVGStyle(String fill, String stroke, double strokeWidth,
			SVGUnit strokeUnit) {
		this.fill = fill;
		this.stroke = stroke;
		this.strokeWidth = strokeWidth;
		this.strokeWidthUnit = strokeUnit;
	}

	/**
	 * 
	 * @return
	 */
	public String toSVG() {
		return String.format(Locale.ENGLISH,
				"style=\"fill:%s;stroke:%s;stroke-width:%.6f%s\"", fill,
				stroke, strokeWidth, strokeWidthUnit);
	}
}
