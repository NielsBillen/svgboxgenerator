package svg;

import core.Box;

/**
 * Interface which should be implemented by all classes which are an element of
 * an SVG.
 * 
 * @author Niels Billen
 * @version 1.0
 */
public interface ISVGElement extends Iterable<ISVGElement> {
	/**
	 * 
	 * @param element
	 * @throws UnsupportedOperationException
	 */
	public void add(ISVGElement element) throws UnsupportedOperationException;

	/**
	 * 
	 * @param element
	 * @throws UnsupportedOperationException
	 */
	public void remove(ISVGElement element)
			throws UnsupportedOperationException;

	/**
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public ISVGElement translate(double x, double y);

	/**
	 * 
	 * @param x
	 * @param y
	 * @param angle
	 * @return
	 */
	public ISVGElement rotate(double x, double y, double angle);

	/**
	 * 
	 * @return
	 */
	public Box getSize();

	/**
	 * 
	 * @param style
	 */
	public void setStyle(SVGStyle style);

	/**
	 * 
	 * @return
	 */
	public SVGStyle getStyle();

	/**
	 * 
	 * @param indent
	 * @return
	 */
	public abstract String toSVG(int indent);
}
