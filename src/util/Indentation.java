package util;

/**
 * 
 * @author Niels Billen
 * @version 1.0
 */
public class Indentation {
	/**
	 * 
	 * @param i
	 * @return
	 */
	public static String getTabs(int i) {
		if (i == 0)
			return "";
		else if (i == 1)
			return "\t";
		else if (i % 2 == 0) {
			String tabs = getTabs(i / 2);
			return tabs + tabs;
		} else
			return "\t" + getTabs(i - 1);
	}
}
