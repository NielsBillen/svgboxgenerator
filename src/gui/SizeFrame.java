package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import svg.SVG;
import svg.SVGUnit;
import box.BoxGenerator;
import core.Box;
import core.Polygon;

/**
 * 
 * @author Niels Billen
 * @version 1.0
 */
public class SizeFrame {
	private JFrame frame;
	private JSlider width;
	private JSlider height;
	private JSlider depth;
	private JSlider thickness;
	private JButton button;

	/**
	 * 
	 */
	public SizeFrame() {
		frame = new JFrame("SVG Box Generator");
		width = createSlider(0, 600, 100, 4, 50);
		height = createSlider(0, 300, 100, 4, 50);
		depth = createSlider(0, 300, 100, 4, 50);
		thickness = createSlider(3, 9, 4, 1, 3);
		thickness.addChangeListener(new ChangeListener() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * javax.swing.event.ChangeListener#stateChanged(javax.swing.event
			 * .ChangeEvent)
			 */
			@Override
			public void stateChanged(ChangeEvent e) {
				width.setMinorTickSpacing(thickness.getValue());
				height.setMinorTickSpacing(thickness.getValue());
				depth.setMinorTickSpacing(thickness.getValue());
				frame.repaint();
			}
		});
		button = new JButton("Generate");

		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		LayoutManager layout = new GridLayout(5, 1);
		panel.setLayout(layout);
		panel.add(createTitledSlider(width, "Breedte:"));
		panel.add(createTitledSlider(height, "Hoogte:"));
		panel.add(createTitledSlider(depth, "Diepte:"));
		panel.add(createTitledSlider(thickness, "Dikte plaat:"));

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		// buttonPanel.setBorder(BorderFactory.createEtchedBorder());
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		buttonPanel.add(button, BorderLayout.CENTER);
		panel.add(buttonPanel);
		frame.add(panel);
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		button.addMouseListener(new MouseListener() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent
			 * )
			 */
			@Override
			public void mouseReleased(MouseEvent e) {

			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent
			 * )
			 */
			@Override
			public void mousePressed(MouseEvent e) {
				List<Polygon> polygons = BoxGenerator.createBox(
						width.getValue(), height.getValue(), depth.getValue(),
						thickness.getValue());
				int index = 0;
				for (Polygon polygon : polygons) {
					Box box = polygon.getSize();
					double ww = box.width() + 2 * thickness.getValue();
					double hh = box.height() + 2 * thickness.getValue();

					SVG svg = new SVG(ww, hh, SVGUnit.MM);
					svg.add(polygon.translate(thickness.getValue(),
							thickness.getValue()));
					svg.save(String.format("svg%d.svg", index));
					++index;
				}

			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent
			 * )
			 */
			@Override
			public void mouseExited(MouseEvent e) {
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent
			 * )
			 */
			@Override
			public void mouseEntered(MouseEvent e) {
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent
			 * )
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
	}

	/**
	 * 
	 * @param slider
	 * @param text
	 * @return
	 */
	private JPanel createTitledSlider(final JSlider slider, final String text) {
		final JPanel panel = new JPanel() {
			private static final long serialVersionUID = 1L;

			public Dimension getPreferredSize() {
				return new Dimension(640, 64);
			};
		};

		final TitledBorder border = BorderFactory.createTitledBorder(text + " "
				+ slider.getValue() + "mm");

		panel.setLayout(new BorderLayout());
		panel.setBorder(border);
		panel.add(slider, BorderLayout.CENTER);

		slider.addChangeListener(new ChangeListener() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * javax.swing.event.ChangeListener#stateChanged(javax.swing.event
			 * .ChangeEvent)
			 */
			@Override
			public void stateChanged(ChangeEvent e) {
				border.setTitle(text + " " + slider.getValue() + "mm");
				panel.repaint();
			}
		});
		return panel;
	}

	/**
	 * 
	 * @return
	 */
	private JSlider createSlider(int min, int max, int initial, int minor,
			int major) {
		JSlider result = new JSlider(JSlider.HORIZONTAL, min, max, initial);
		// result.setMajorTickSpacing(major);
		result.setMinorTickSpacing(minor);
		result.setPaintTicks(true);
		result.setPaintLabels(true);
		result.setSnapToTicks(true);
		return result;
	}
}
