package box;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import core.Line;
import core.Polygon;

/**
 * 
 * @author Niels Billen
 * @version 1.0
 */
public class BoxGenerator {
	/**
	 * 
	 * @param crennelationSize
	 * @return
	 */
	public static List<Polygon> createBox(double width, double height,
			double depth, double thickness) {
		double inv_thickness = 1.0 / thickness;

		int wi = (int) (width * inv_thickness);
		int hi = (int) (height * inv_thickness);
		int di = (int) (depth * inv_thickness);
		wi = wi % 2 == 0 ? wi + 1 : wi;
		hi = hi % 2 == 0 ? hi + 1 : hi;
		di = di % 2 == 0 ? di + 1 : di;

		double w = wi * thickness;
		double h = hi * thickness;
		double d = di * thickness;

		if (w != width)
			System.err.format(Locale.ENGLISH,
					"rounded width from %.6f to %.6f to be an uneven multiple of "
							+ "the thickness!\n", width, w);
		if (h != height)
			System.err.format(Locale.ENGLISH,
					"rounded height from %.6f to %.6f to be an uneven multiple of "
							+ "the thickness!\n", height, h);
		if (d != depth)
			System.err.format(Locale.ENGLISH,
					"rounded depth from %.6f to %.6f to be an uneven multiple of "
							+ "the thickness!\n", depth, d);
		List<Polygon> result = new ArrayList<Polygon>();
		result.add(constructFront(wi, hi, thickness));
		result.add(constructSide(di, hi, thickness));
		result.add(constructTop(wi, di, thickness));

		// Generate the front polygon basic outline

		return result;
	}

	/**
	 * 
	 * @param width
	 * @return
	 */
	public static Line getType1(int count, double thickness) {
		if (count % 2 == 0)
			throw new IllegalStateException();

		// Construct the line.
		Line line = new Line();
		line.add(0, 0);
		line.add(thickness, 0);
		double t2 = 2.0 * thickness;
		for (int i = 0; i < count / 2; ++i) {
			double xx = thickness + i * t2;
			line.add(xx, 0.f);
			line.add(xx + thickness, 0.f);
			line.add(xx + thickness, thickness);
			line.add(xx + t2, thickness);
		}

		line.add(count * thickness, 0.f);
		line.add(count * thickness + 2 * thickness, 0.f);

		return line;
	}

	/**
	 * 
	 * @param width
	 * @return
	 */
	public static Line getType2(int count, double thickness) {
		if (count % 2 == 0)
			throw new IllegalStateException();

		// Construct the line.
		Line line = new Line();
		for (int i = 0; i < count / 2; ++i) {
			double xx = (2 * i + 1) * thickness;
			line.add(xx, thickness);
			line.add(xx + thickness, thickness);
			line.add(xx + thickness, 0.f);
			line.add(xx + 2 * thickness, 0.f);
		}
		line.add((count) * thickness, thickness);
		line.add((count + 1) * thickness, thickness);

		return line;
	}

	/**
	 * 
	 * @param width
	 * @return
	 */
	public static Line getType3(int count, double thickness) {
		if (count % 2 == 0)
			throw new IllegalStateException();

		// Construct the line.
		Line line = new Line();
		line.add(0, thickness);
		for (int i = 0; i < count / 2; ++i) {
			double xx = (2 * i + 1) * thickness;
			line.add(xx, thickness);
			line.add(xx + thickness, thickness);
			line.add(xx + thickness, 0.f);
			line.add(xx + 2 * thickness, 0.f);
		}
		line.add((count) * thickness, thickness);
		line.add((count + 2) * thickness, thickness);

		return line;
	}

	/**
	 * 
	 * @param width
	 * @return
	 */
	public static Line getType4(int count, double thickness) {
		if (count % 2 == 0)
			throw new IllegalStateException();

		// Construct the line.
		Line line = new Line();
		line.add(thickness, 0);
		double t2 = 2.0 * thickness;
		for (int i = 0; i < count / 2; ++i) {
			double xx = thickness + i * t2;
			line.add(xx, 0.f);
			line.add(xx + thickness, 0.f);
			line.add(xx + thickness, thickness);
			line.add(xx + t2, thickness);
		}

		line.add(count * thickness, 0.f);
		line.add(count * thickness + thickness, 0.f);

		return line;
	}

	/**
	 * 
	 * @param wi
	 * @param hi
	 * @return
	 */
	public static Polygon constructFront(int wi, int hi, double thickness) {
		Line e1 = getType1(wi, thickness);
		Line e2 = getType1(hi, thickness);
		Polygon result = new Polygon();

		double xx = (wi + 2) * thickness;
		double yy = (hi + 2) * thickness;

		result.append(e1);
		result.append(e2.rotate(0, 0, 90).translate(xx, 0));
		result.append(e1.rotate(0, 0, 180).translate(xx, yy));
		result.append(e2.rotate(0, 0, 270).translate(0, yy));

		return result;
	}

	/**
	 * 
	 * @param wi
	 * @param hi
	 * @return
	 */
	public static Polygon constructSide(int wi, int hi, double thickness) {
		Line e1 = getType2(wi, thickness);
		Line e2 = getType2(hi, thickness);
		Polygon result = new Polygon();

		double xx = (wi + 2) * thickness;
		double yy = (hi + 2) * thickness;

		result.append(e1);
		result.append(e2.rotate(0, 0, 90).translate(xx, 0));
		result.append(e1.rotate(0, 0, 180).translate(xx, yy));
		result.append(e2.rotate(0, 0, 270).translate(0, yy));

		return result;
	}

	/**
	 * 
	 * @param wi
	 * @param hi
	 * @return
	 */
	public static Polygon constructTop(int wi, int hi, double thickness) {
		Line e1 = getType3(wi, thickness);
		Line e2 = getType4(hi, thickness);
		Polygon result = new Polygon();

		double xx = (wi + 2) * thickness;
		double yy = (hi + 2) * thickness;

		result.append(e1);
		result.append(e2.rotate(0, 0, 90).translate(xx, 0));
		result.append(e1.rotate(0, 0, 180).translate(xx, yy));
		result.append(e2.rotate(0, 0, 270).translate(0, yy));

		return result;
	}
}
