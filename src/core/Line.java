package core;


/**
 * 
 * @author Niels Billen
 * @version 1.0
 */
public class Line extends PointList {
	/**
	 * 
	 */
	public Line() {
		super();
	}

	/**
	 * 
	 * @param line
	 */
	public Line(Line line) {
		super(line);
	}

	/**
	 * 
	 * @param iterable
	 */
	public Line(Iterable<Point2D> iterable) {
		super(iterable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.PointList#getSVGElementName()
	 */
	@Override
	public String getSVGElementName() {
		return "polyline";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#translate(double, double)
	 */
	@Override
	public Line translate(double x, double y) {
		return new Line(translatePoints(x, y));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#rotate(double, double, double)
	 */
	@Override
	public Line rotate(double x, double y, double angle) {
		return new Line(rotatePoints(x, y, angle));
	}
}
