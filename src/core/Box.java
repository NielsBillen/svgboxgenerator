package core;

/**
 * 
 * @author Niels Billen
 * @version 1.0
 */
public class Box {
	public final Point2D min;
	public final Point2D max;

	/**
	 * 
	 */
	public Box() {
		this(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY,
				Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
	}

	/**
	 * 
	 * @param min
	 * @param max
	 */
	public Box(Point2D min, Point2D max) {
		this.min = min;
		this.max = max;
	}

	/**
	 * 
	 * @param minx
	 * @param miny
	 * @param maxx
	 * @param maxy
	 */
	public Box(double minx, double miny, double maxx, double maxy) {
		this(new Point2D(minx, miny), new Point2D(maxx, maxy));
	}

	/**
	 * 
	 * @return
	 */
	public double width() {
		return max.x - min.x;
	}

	/**
	 * 
	 * @return
	 */
	public double height() {
		return max.y - min.y;
	}

	/**
	 * 
	 * @param box
	 * @return
	 */
	public Box union(Box box) {
		double minx = Math.min(min.x, box.min.x);
		double miny = Math.min(min.y, box.min.y);
		double maxx = Math.max(max.x, box.max.x);
		double maxy = Math.max(max.y, box.max.y);
		return new Box(minx, miny, maxx, maxy);
	}
}
