package core;

/**
 * 
 * @author Niels Billen
 * @version 1.0
 */
public class Point2D {
	public final double x;
	public final double y;

	/**
	 * 
	 */
	public Point2D() {
		x = 0;
		y = 0;
	}

	/**
	 * 
	 * @param x
	 * @param y
	 */
	public Point2D(double x, double y) throws NullPointerException {
		this.x = x;
		this.y = y;
	}
}
