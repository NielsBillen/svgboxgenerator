package core;

import svg.ISVGElement;

/**
 * 
 * @author Niels Billen
 * @version 1.0
 */
public class Polygon extends PointList implements ISVGElement {
	/**
	 * 
	 */
	public Polygon() {
		super();
	}

	/**
	 * 
	 * @param polygon
	 */
	public Polygon(Polygon polygon) {
		super(polygon);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.PointList#getSVGElementName()
	 */
	@Override
	public String getSVGElementName() {
		return "polygon";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#translate(double, double)
	 */
	@Override
	public ISVGElement translate(double x, double y) {
		return new Line(translatePoints(x, y));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#rotate(double, double, double)
	 */
	@Override
	public ISVGElement rotate(double x, double y, double angle) {
		return new Line(rotatePoints(x, y, angle));
	}
}
