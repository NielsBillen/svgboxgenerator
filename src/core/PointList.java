package core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;

import svg.ISVGElement;
import svg.SVGStyle;

/**
 * 
 * @author Niels Billen
 * @version 1.0
 */
public abstract class PointList implements ISVGElement {
	private final List<Point2D> points = new ArrayList<Point2D>();
	private SVGStyle style = SVGStyle.DEFAULT;

	/**
	 * 
	 */
	public PointList() {
	}

	/**
	 * 
	 * @param list
	 */
	public PointList(PointList list) {
		if (list == null)
			throw new NullPointerException("the given list is null!");
		for (Point2D point : list.points())
			add(point);
	}

	/**
	 * 
	 * @param list
	 */
	public PointList(Iterable<Point2D> iterable) {
		if (iterable == null)
			throw new NullPointerException("the given list is null!");
		for (Point2D point : iterable)
			add(point);
	}

	/**
	 * 
	 * @param point
	 * @throws NullPointerException
	 */
	public void add(Point2D point) throws NullPointerException {
		if (point == null)
			throw new NullPointerException();
		points.add(point);
	}

	/**
	 * 
	 * @param x
	 * @param y
	 */
	public void add(double x, double y) {
		add(new Point2D(x, y));
	}

	/**
	 * 
	 * @param point
	 * @return
	 * @throws NullPointerException
	 */
	public boolean remove(Point2D point) throws NullPointerException {
		return points.remove(point);
	}

	/**
	 * 
	 * @param index
	 * @return
	 * @throws ArrayIndexOutOfBoundsException
	 */
	public Point2D remove(int index) throws ArrayIndexOutOfBoundsException {
		return points.remove(index);
	}

	/**
	 * 
	 * @param list
	 */
	public void append(PointList list) {
		if (list == null)
			throw new NullPointerException();
		for (Point2D point : list.points())
			add(point);
	}

	/**
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	protected List<Point2D> translatePoints(double x, double y) {
		List<Point2D> result = new ArrayList<Point2D>();

		for (Point2D point : points())
			result.add(new Point2D(x + point.x, y + point.y));
		return result;
	}

	/**
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	protected List<Point2D> rotatePoints(double x, double y, double angle) {
		List<Point2D> result = new ArrayList<Point2D>();

		for (Point2D point : points()) {
			Point2D t = new Point2D(point.x - x, point.y - y);
			double r = Math.sqrt(t.x * t.x + t.y * t.y);
			double a = Math.atan2(t.y, t.x);
			double aa = a + Math.toRadians(angle);
			double xx = x + r * Math.cos(aa);
			double yy = y + r * Math.sin(aa);
			result.add(new Point2D(xx, yy));
		}
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Point2D> points() {
		return new ArrayList<Point2D>(points);
	}

	/**
	 * 
	 * @return
	 */
	public abstract String getSVGElementName();

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#add(svg.ISVGElement)
	 */
	@Override
	public void add(ISVGElement element) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#remove(svg.ISVGElement)
	 */
	@Override
	public void remove(ISVGElement element)
			throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#setStyle(svg.SVGStyle)
	 */
	@Override
	public void setStyle(SVGStyle style) {
		if (style == null)
			throw new NullPointerException();
		this.style = style;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#getStyle()
	 */
	@Override
	public SVGStyle getStyle() {
		return style;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#toSVG(int)
	 */
	@Override
	public String toSVG(int indent) {
		StringBuilder builder = new StringBuilder();
		StringBuilder tabs = new StringBuilder();
		for (int i = 0; i < indent; ++i)
			tabs.append("\t");
		builder.append(tabs);
		builder.append("<path");
		builder.append(" d=\"M ");
		for (Point2D point : points()) {
			builder.append("\n\t");
			builder.append(tabs);
			builder.append(String.format(Locale.ENGLISH, " %f,%f", point.x,
					point.y));
		}
		builder.append(" z \"");
		builder.append("\n");
		builder.append(tabs);

		builder.append(getStyle().toSVG());
		builder.append(" />");
		return builder.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see svg.ISVGElement#getSize()
	 */
	@Override
	public Box getSize() {
		double minx = Double.POSITIVE_INFINITY;
		double miny = Double.POSITIVE_INFINITY;
		double maxx = Double.NEGATIVE_INFINITY;
		double maxy = Double.NEGATIVE_INFINITY;
		for (Point2D point : points()) {
			minx = Math.min(minx, point.x);
			miny = Math.min(miny, point.y);
			maxx = Math.max(maxx, point.x);
			maxy = Math.max(maxy, point.y);
		}
		return new Box(minx, miny, maxx, maxy);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<ISVGElement> iterator() {
		return new Iterator<ISVGElement>() {
			private boolean hasNext = true;

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Iterator#hasNext()
			 */
			@Override
			public boolean hasNext() {
				return hasNext;
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Iterator#next()
			 */
			@Override
			public ISVGElement next() {
				if (hasNext) {
					hasNext = false;
					return PointList.this;
				}
				throw new NoSuchElementException();
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Iterator#remove()
			 */
			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}
}
